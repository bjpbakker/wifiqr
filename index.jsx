// This file is part of WifiQR.
//
// WifiQR is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// WifiQR is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with WifiQR.  If not, see <https://www.gnu.org/licenses/>.

import { compose } from "@fpjs/overture/base";
import { patchBuiltins } from "@fpjs/overture/patches";
import { App } from "@fpjs/signal";
const { Eff, start, noEffects, transition } = App;

import {set, Record} from "@fpjs/overture/control/lens";
const _ = Record;

import { Component, h, render} from "preact";

import { default as QRCode } from "qrcode-svg";

const SetSSID = (e) => ({
    kind: "SetSSID",
    value: e.target.value
});

const SetPreSharedKey = (e) => ({
    kind: "SetPreSharedKey",
    value: e.target.value
});

const SetAuthenticationType = (e) => ({
    kind: "SetAuthenticationType",
    value: e.target.value
});

const GenerateQR = () => ({
    kind: "GenerateQR",
});

const WifiForm = ({formdata: {ssid, preSharedKey, authenticationType}, input}) => (
    <div class="inputs">
        <label for="authenticationType">Authentication Type</label>
        <select id="authenticationType" onchange={compose (input) (SetAuthenticationType)}>
          <option value="WEP" selected={authenticationType === 'WEP'}>WEP</option>
          <option value="WPA" selected={authenticationType === 'WPA'}>WPA</option>
        </select>
        <label for="ssid">SSID</label>
        <input id="ssid" autofocus value={ssid} type="text" placeholder="My Network"
               onkeyup={compose (input) (SetSSID)}
               onchange={compose (input) (SetSSID)}
        />
        <label for="preSharedKey">Pre-Shared Key</label>
        <input id="preSharedKey" type="text" value={preSharedKey} placeholder="Secret" maxlength="63"
            onkeyup={compose (input) (SetPreSharedKey)}
            onchange={compose (input) (SetPreSharedKey)}
        />

        <div class="actions">
            <input type="button" value="Generate" onclick={compose (input) (GenerateQR)} />
        </div>
    </div>
);

const Application = ({state, input}) => (
    <div>
        <WifiForm formdata={state.wifiForm} input={input} />
        {state.qrcode && <img class="qr-code" src={`data:image/svg+xml;base64,${btoa(state.qrcode)}`} />}
    </div>
);

const onlyHexChars = (s) => /^[0-9A-Fa-f]+$/.test(s);

const encode = (s) => {
    if (onlyHexChars(s)) {
        return `"${s}"`;
    } else {
        return s.replace(/(?=[\\;,:"])/g, '\\');
    }
};

const foldp = (event) => {
    switch (event.kind) {
    case "SetAuthenticationType":
        return transition (set (_.wifiForm.authenticationType) (event.value));
    case "SetSSID":
        return transition (set (_.wifiForm.ssid) (event.value));
    case "SetPreSharedKey":
        return transition (set (_.wifiForm.preSharedKey) (event.value));
    case "GenerateQR": return (state) => {
        const {authenticationType, ssid, preSharedKey} = state.wifiForm;
        const content = `WIFI:T:${authenticationType};S:${encode(ssid)};P:${encode(preSharedKey)};;`;
        const svg = new QRCode({
            content,
            xmlDeclaration: false,
        }).svg();
        return transition (set (_.qrcode) (svg)) (state);
    };
    default:
        return noEffects;
    }
};

const run = (root) => {
    const view = Eff(({state, input}) => render(<Application state={state} input={input} />, root, null));

    const initState = {
        wifiForm: {
            ssid: '',
            preSharedKey: '',
            authenticationType: 'WPA',
        },
        qrcode: '',
    };

    const config = {
        initState: initState,
        foldp: foldp,
        render: view,
        inputs: []
    };

    const app = start (config);
};

const main = () => {
    const root = document.getElementById("app");
    if (root != null) {
        run(root);
    } else {
        console.log("Waiting for app root to render..");
        setTimeout(main, 10);
    }
};

patchBuiltins ();
main ();
